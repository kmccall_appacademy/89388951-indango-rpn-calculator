class RPNCalculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def tokens(string)
    tokens = string.split
    tokens.map { |el| operations?(el) ? el.to_sym : Integer(el) }
  end

  def evaluate(string)
    tokens = tokens(string)

    tokens.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
    end
    value
  end

  private

  def operations?(el)
    [:+, :-, :/, :*].include?(el.to_sym)
  end

  def perform_operation(op_symbol)
    raise "calculator is empty" if @stack.size < 2

    second_variable = @stack.pop
    first_variable = @stack.pop

    case op_symbol
    when :+
      @stack << first_variable + second_variable
    when :-
      @stack << first_variable - second_variable
    when :*
      @stack << first_variable * second_variable
    when :/
      @stack << first_variable.fdiv(second_variable)
    else
      @stack << first_variable
      @stack << second_variable
      raise "No such operation: #{op_symblo}"
    end
  end
end
